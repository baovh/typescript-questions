export default class Bowling {
    private rolls: number[];

    constructor(rolls: number[]) {
        this.rolls = rolls;
    }

    score(): number {
        return this.getScore(this.rolls);
    }

    private getScore(rolls: number[]): number {
        let scoreTotal: number = 0;
        if (rolls.length < 10) {
            throw new Error("Score cannot be taken until the end of the game");
        }

        if (rolls[20] !== undefined && rolls[18] + rolls[19] < 10) {
            throw new Error("Should not be able to roll after game is over");
        }

        if (rolls[18] === 10 && (rolls[19] === undefined || rolls[20] === undefined)) {
            throw new Error("Score cannot be taken until the end of the game");
        }

        if (rolls[18] + rolls[19] >= 10 && rolls[20] === undefined) {
            throw new Error("Score cannot be taken until the end of the game");
        }

        for (let i = 0; i < rolls.length; i++) {
            if (rolls[i] < 0 || rolls[i] > 10) {
                throw new Error("Pins must have a value from 0 to 10");
            }

            if (i === 19) {
                if (rolls[i - 1] === 10) {
                    break;
                }

                if (rolls[i] < 10) {
                    scoreTotal += rolls[i];
                    break;
                }
            }

            if (rolls[i] === 10) {
                scoreTotal += this.getSpareScore(i);
                if (i === rolls.length - 3) {
                    if (rolls[i + 1] != 10 && rolls[i + 2] != 10 && rolls[i + 1] + rolls[i + 2] > 10
                    ) {
                        throw new Error("Pin count exceeds pins on the lane");
                    }

                    if (rolls[i + 1] != 10 && rolls[i + 2] >= 10) {
                        throw new Error("Pin count exceeds pins on the lane");
                    }
                    break;
                }
                continue;
            }

            if (this.getNornalScore(i) > 10) {
                throw new Error("Pin count exceeds pins on the lane");
            }

            if (this.getNornalScore(i) < 10) {
                scoreTotal += this.getNornalScore(i);
                i++;
                continue;
            }

            if (this.getNornalScore(i) === 10) {
                scoreTotal += this.getSpareScore(i);
                if (i === 18) {
                    break;
                }
                i++;
                continue;
            }
        }
        return scoreTotal;
    }

    private getNornalScore(rollIndex: number): number {
        return this.rolls[rollIndex] + this.rolls[rollIndex + 1];
    }

    private getSpareScore(rollIndex: number): number {
        return this.rolls[rollIndex] + this.rolls[rollIndex + 1] + this.rolls[rollIndex + 2];
    }
}