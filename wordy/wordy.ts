export class WordProblem {

    stringInput: string;
    constructor(stringValue: string) {
        // remove "?" from Quest
        if (stringValue.slice(-1) == "?")
            stringValue = stringValue.slice(0, stringValue.length - 1);
        this.stringInput = stringValue;
    }

    answer(): any {
        const methodConst: string[] = ["plus", "minus", "multiplied", "divided", "power"];

        var result: number = 0;
        var indexCheckSynyax: boolean = false;
        var resultArray: number[] = [];
        var numberInput: number[] = [];
        var methodInput: string[] = [];
        // Split String to Array
        var stringAfterSplitted: string[] = this.stringInput.split(" ");
        // Loop to collect numberInput and method
        stringAfterSplitted.forEach((item, index) => {
            // Convert string to number
            var itemSelect = +item;
            // collect number
            if (!isNaN(itemSelect)) {
                numberInput.push(itemSelect);
            }
            // collect method
            if (methodConst.includes(item)) {
                methodInput.push(item);
                if (methodConst.includes(stringAfterSplitted[index + 1])) {
                    indexCheckSynyax = true;
                }
            }
        });

        // Validate 
        // Check Non-math 
        if (methodInput.length == 0) {
            throw new ArgumentError("Non-math questions");
        }
        // Check Unsupported operations
        if (numberInput.length == 1 && stringAfterSplitted.includes("cubed")) {
            throw new ArgumentError("Unsupported operations");
        }
        // Check syntax
        if (indexCheckSynyax) {
            throw new ArgumentError("Word problems with invalid syntax");
        }
        if (methodInput.includes(methodConst[4])) {
            // BONUS.
            var valuePower = +stringAfterSplitted[stringAfterSplitted.length - 2].slice(0, -2);
            numberInput.push(valuePower);
        }
        // 
        resultArray = numberInput;

        // Caculate from left to right.
        result = resultArray[0];
        methodInput.forEach((item, index) => {
            if (item == methodConst[0]) {
                result = result + resultArray[index + 1];
            } else if (item == methodConst[1]) {
                result = result - resultArray[index + 1];
            } else if (item == methodConst[2]) {
                result = result * resultArray[index + 1];
            } else if (item == methodConst[3]) {
                result = result / resultArray[index + 1];
            } else if (item == methodConst[4]) {
                result = Math.pow(result, resultArray[index + 1]);
            }
        });

        return result;
    }
}

export class ArgumentError extends Error {
    constructor(message: string) {
        super(message);
    }
}